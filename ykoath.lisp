;; stump-ykoauth is a StumpWM module for querying the Yubikey OATH application
;; Copyright (C) 2022 Raffael Stocker, <r.stocker@mnet-mail.de>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary
;; To query the Yubikey OATH application, this module uses the ‘ykman’
;; program.  It is assumed that the access pin for the OATH application
;; is cached and therefore doesn't need to be entered.
;;
;; Usage:
;; Call the ‘oath-code’ command and select an account.  The OATH code
;; will be displayed and copied to the clipboard.

(in-package #:ykoath)

(defparameter *passwd* nil)

(defun maybe-read-passwd ()
  (unless *passwd*
    (setf *passwd* (read-one-line (current-screen)
                                  "Yubikey password: "
                                  :password t))))

(defun read-account (&optional initial-input)
  (maybe-read-passwd)
  (let ((account-list (split-string
                       (run-shell-command
                        (concatenate 'string "ykman oath accounts list -p " *passwd*) t))))
    (when account-list
      (completing-read (current-screen) "Select account: "
                       account-list
                       :initial-input (or initial-input "")))))

(defcommand rm-yubikey-passwd () (:rest)
  (setf *passwd* nil))

(defcommand oath-code (&optional initial-input) (:rest)
  (maybe-read-passwd)
  (let ((account (read-account initial-input)))
    (when (and account (string/= "" account))
      (let ((code (run-shell-command
                   (concatenate 'string
                                "ykman oath accounts code -p "
                                *passwd*
                                " "
                                (concatenate 'string "'"
                                             account "'"))
                   t)))
        (echo code)
        (set-x-selection (car (last (split-string code "[ \n]+"))) :clipboard)))))
