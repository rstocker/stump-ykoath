;; stump-ykoauth is a StumpWM module for querying the Yubikey OATH application
;; Copyright (C) 2022 Raffael Stocker, <r.stocker@mnet-mail.de>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:ykoath
  :description "Use Yubikey OATH-TOTP."
  :author "Raffael Stocker <r.stocker@mnet-mail.de>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:stumpwm)
  :components ((:file "package")
               (:file "ykoath")))
