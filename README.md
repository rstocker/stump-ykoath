# stump-ykoath

  This StumpWM module displays OATH-TOTP codes using a yubikey and `ykman` as
  the backend.

  To query the Yubikey OATH application, this module calls the `ykman` program
  in a shell process.  It is assumed that the access pin for the OATH
  application is cached and therefore doesn't need to be entered.

  To use it, call the `oath-code` command and select an account.  The OATH
  code will be displayed and copied to the clipboard.


## License

   Licensed under GNU General Public License, Version 3 or later.  See the
   file LICENSE for details.
